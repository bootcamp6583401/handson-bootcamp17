package task.two.lesson;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.Student;
import task.two.lesson.models.Teacher;
import task.two.lesson.repositories.StudentRepository;
import task.two.lesson.repositories.TeacherRepository;
import task.two.lesson.services.impl.StudentServiceImpl;
import task.two.lesson.services.impl.TeacherServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TeacherTest {
    @Mock
    private TeacherRepository repo;

    @InjectMocks
    private TeacherServiceImpl service;

    private Teacher teacher;

    @BeforeEach
    public void setup() {
        teacher = new Teacher();
        teacher.setId(333);
        teacher.setBirthDate(LocalDate.now());
        teacher.setFirstName("Michael");
        teacher.setLastName("Scott");
        teacher.setGender(Teacher.Gender.MALE);
    }

    @Test
    public void saveandreturn() {
        Teacher teacher = new Teacher();
        LocalDate nowBro = LocalDate.now();
        teacher.setFirstName("Test Name");
        teacher.setBirthDate(nowBro);
        teacher.setGender(Teacher.Gender.FEMALE);
        teacher.setLastName("Last name");
        when(repo.save(ArgumentMatchers.any(Teacher.class))).thenReturn(teacher);
        Teacher created = service.save(teacher);
        assertEquals(teacher.getFirstName(), created.getFirstName());
        assertEquals(teacher.getLastName(), created.getLastName());
        assertEquals(teacher.getGender(), created.getGender());
        assertEquals(teacher.getBirthDate(), created.getBirthDate());
        assertTrue(teacher.equals(created));
        assertEquals(teacher.hashCode(), created.hashCode());
        verify(repo).save(teacher);
    }

    // JUnit test for getAllTeachers method
    @DisplayName("JUnit test for getAllTeachers method")
    @Test
    public void givenTeachersList_whenGetAllTeachers_thenReturnTeachersList() {
        // given - precondition or setup

        Teacher teach1 = new Teacher();
        Teacher teach2 = new Teacher();

        when(repo.findAll()).thenReturn(List.of(teach1, teach2));

        // when - action or the behaviour that we are going test
        List<Teacher> teachers = StreamSupport.stream(service.getAll().spliterator(), false)
                .collect(Collectors.toList());
        ;

        // then - verify the output
        assertNotNull(teachers);
        assertEquals(2, teachers.size());
    }

    // JUnit test for getTeacherById method
    @DisplayName("JUnit test for getTeacherById method")
    @Test
    public void givenTeacherId_whenGetTeacherById_thenReturnTeacherObject() {

        Teacher teach = new Teacher();
        teach.setId(89);
        when(repo.findById(teach.getId())).thenReturn(Optional.of(teach));
        Teacher expected = service.getById(teach.getId());
        assertEquals(expected, teach);
        verify(repo).findById(teach.getId());

    }

    @Test
    public void whenGivenId_shouldDeleteTeacher_ifFound() {
        Teacher teacher = new Teacher();
        teacher.setFirstName("Test Name");
        teacher.setId(3444);
        service.deleteById(teacher.getId());
        verify(repo).deleteById(teacher.getId());
        verify(repo, times(1)).deleteById(teacher.getId());

    }

    @Test
    public void whenGivenId_shouldUpdateTeacher_ifFound() {
        Teacher teach = new Teacher();
        teach.setId(893);
        teach.setFirstName("Test Name");
        Teacher newTeacher = new Teacher();
        teach.setFirstName("New Test Name");
        when(repo.findById(teach.getId())).thenReturn(Optional.of(teach));
        service.updateById(teach.getId(), newTeacher);
        verify(repo).save(newTeacher);
        verify(repo).findById(teach.getId());
    }

    // // JUnit test for updateTeacher method
    // @DisplayName("JUnit test for updateTeacher method")
    // @Test
    // public void givenTeacherObject_whenUpdateTeacher_thenReturnUpdatedTeacher(){
    // // given - precondition or setup
    // given(repo.save(employee)).willReturn(employee);
    // employee.setEmail("ram@gmail.com");
    // employee.setFirstName("Ram");
    // // when - action or the behaviour that we are going test
    // Teacher updatedTeacher = employeeService.updateTeacher(employee);

    // // then - verify the output
    // assertThat(updatedTeacher.getEmail()).isEqualTo("ram@gmail.com");
    // assertThat(updatedTeacher.getFirstName()).isEqualTo("Ram");
    // }

    // // JUnit test for deleteTeacher method
    // @DisplayName("JUnit test for deleteTeacher method")
    // @Test
    // public void givenTeacherId_whenDeleteTeacher_thenNothing(){
    // // given - precondition or setup
    // long employeeId = 1L;

    // willDoNothing().given(repo).deleteById(employeeId);

    // // when - action or the behaviour that we are going test
    // employeeService.deleteTeacher(employeeId);

    // // then - verify the output
    // verify(repo, times(1)).deleteById(employeeId);
    // }
}
