package task.two.lesson;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.Student;
import task.two.lesson.repositories.StudentRepository;
import task.two.lesson.services.impl.StudentServiceImpl;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class StudentTest {
    @Autowired
    private StudentRepository repo;

    @Test
    public void testAdd() {
        Student student = new Student();

        student.setFirstName("Kevinza");

        student.setLastName("Malone");

        student.setGender(Student.Gender.MALE);

        student.setClassName("Astronomy");

        Student savedObj = repo.save(student);
        Student foundStudent = repo.findById(savedObj.getId())
                .orElseThrow(() -> new EntityNotFoundException("Dumbass"));

        Assertions.assertTrue(savedObj.equals(foundStudent));
        Assertions.assertEquals(savedObj.hashCode(), foundStudent.hashCode());

        Assertions.assertTrue(savedObj.getId() > 0);
        Assertions.assertNotNull(savedObj);
    }

    @Test
    public void saveAndFindByFirstName() {
        Student student = new Student();

        student.setFirstName("Kevinza");

        student.setLastName("Malone");

        student.setGender(Student.Gender.MALE);

        student.setClassName("Astronomy");

        repo.save(student);

        Student foundStudent = repo.findAllByFirstName("Kevinza");

        Assertions.assertEquals("Kevinza", foundStudent.getFirstName());
    }

    @Test
    public void update() {
        Student student = new Student();

        student.setFirstName(LocalDateTime.now().toString());

        student.setLastName("Malone");

        student.setGender(Student.Gender.MALE);

        student.setClassName("Astronomy");

        Student foundStudent = repo.findOne();

        String name = foundStudent.getFirstName();

        student.setId(foundStudent.getId());

        Student savedStudent = repo.save(student);

        Assertions.assertEquals(savedStudent.getId(), foundStudent.getId());
        Assertions.assertNotEquals(name, savedStudent.getFirstName());

    }
}
