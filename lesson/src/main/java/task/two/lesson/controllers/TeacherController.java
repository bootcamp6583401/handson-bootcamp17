
package task.two.lesson.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import task.two.lesson.models.Teacher;
import task.two.lesson.services.impl.TeacherServiceImpl;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    TeacherServiceImpl service;

    @GetMapping
    public Iterable<Teacher> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Teacher save(@RequestBody Teacher product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Teacher getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Teacher updateById(@PathVariable int id, @RequestBody Teacher product) {
        return service.updateById(id, product);
    }

}