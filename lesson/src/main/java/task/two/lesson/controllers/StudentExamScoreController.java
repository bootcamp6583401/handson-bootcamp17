
package task.two.lesson.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import task.two.lesson.models.StudentExamScore;
import task.two.lesson.services.impl.StudentExamScoreServiceImpl;

@RestController
@RequestMapping("/scores")
public class StudentExamScoreController {

    @Autowired
    StudentExamScoreServiceImpl service;

    @GetMapping
    public Iterable<StudentExamScore> getAll() {
        return service.getAll();
    }

    @PostMapping
    public StudentExamScore save(@RequestBody StudentExamScore product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public StudentExamScore getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public StudentExamScore updateById(@PathVariable int id, @RequestBody StudentExamScore product) {
        return service.updateById(id, product);
    }

}