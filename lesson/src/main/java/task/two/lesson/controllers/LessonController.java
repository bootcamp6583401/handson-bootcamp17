
package task.two.lesson.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import task.two.lesson.models.Lesson;
import task.two.lesson.services.impl.LessonServiceImpl;

@RestController
@RequestMapping("/lessons")
public class LessonController {

    @Autowired
    LessonServiceImpl service;

    @GetMapping
    public Iterable<Lesson> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Lesson save(@RequestBody Lesson product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Lesson getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Lesson updateById(@PathVariable int id, @RequestBody Lesson product) {
        return service.updateById(id, product);
    }

}