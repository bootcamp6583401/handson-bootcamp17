
package task.two.lesson.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import task.two.lesson.models.StudentLessonSchedule;
import task.two.lesson.services.impl.StudentLessonScheduleServiceImpl;

@RestController
@RequestMapping("/student-schedules")
public class StudentLessonScheduleController {

    @Autowired
    StudentLessonScheduleServiceImpl service;

    @GetMapping
    public Iterable<StudentLessonSchedule> getAll() {
        return service.getAll();
    }

    @PostMapping
    public StudentLessonSchedule save(@RequestBody StudentLessonSchedule product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public StudentLessonSchedule getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public StudentLessonSchedule updateById(@PathVariable int id, @RequestBody StudentLessonSchedule product) {
        return service.updateById(id, product);
    }

}