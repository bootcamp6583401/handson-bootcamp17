
package task.two.lesson.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import task.two.lesson.models.Exam;
import task.two.lesson.services.impl.ExamServiceImpl;

@RestController
@RequestMapping("/exams")
public class ExamController {

    @Autowired
    ExamServiceImpl service;

    @GetMapping
    public Iterable<Exam> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Exam save(@RequestBody Exam product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public Exam getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Exam updateById(@PathVariable int id, @RequestBody Exam product) {
        return service.updateById(id, product);
    }

}