
package task.two.lesson.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import task.two.lesson.models.TeacherLessonSchedule;
import task.two.lesson.services.impl.TeacherLessonScheduleServiceImpl;

@RestController
@RequestMapping("/teacher-schedules")
public class TeacherLessonScheduleController {

    @Autowired
    TeacherLessonScheduleServiceImpl service;

    @GetMapping
    public Iterable<TeacherLessonSchedule> getAll() {
        return service.getAll();
    }

    @PostMapping
    public TeacherLessonSchedule save(@RequestBody TeacherLessonSchedule product) {
        return service.save(product);
    }

    @GetMapping("/{id}")
    public TeacherLessonSchedule getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public TeacherLessonSchedule updateById(@PathVariable int id, @RequestBody TeacherLessonSchedule product) {
        return service.updateById(id, product);
    }

}