package task.two.lesson.models;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Data;

@Entity
@Data
public class Exam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private LocalDate examDate;

    private LocalTime examTime;

    @ManyToOne
    @JoinColumn(name = "lesson_id", referencedColumnName = "id")
    @JsonBackReference(value = "lesson-exams")
    private Lesson lesson;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "exam")
    @JsonManagedReference(value = "exam-scores")
    private List<StudentExamScore> studentExamScores;

}
