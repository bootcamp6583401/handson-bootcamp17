package task.two.lesson.models;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

@Entity
@Data
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;

    private String lastName;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String className;

    public static enum Gender {
        MALE,
        FEMALE
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
    @JsonManagedReference(value = "student-scores")
    private List<StudentExamScore> studentExamScores;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
    @JsonManagedReference(value = "student-lesson-schedules")
    private List<StudentLessonSchedule> studentLessonSchedules;

}
