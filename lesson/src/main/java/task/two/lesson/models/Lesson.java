package task.two.lesson.models;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

@Entity
@Data
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String subject;

    private String className;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lesson")
    @JsonManagedReference(value = "lesson-exams")
    private List<Exam> exams;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lesson")
    @JsonManagedReference(value = "lesson-teacher-schedules")
    private List<TeacherLessonSchedule> teacherLessonSchedules;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lesson")
    @JsonManagedReference(value = "lesson-student-schedules")
    private List<StudentLessonSchedule> studentLessonSchedules;

}
