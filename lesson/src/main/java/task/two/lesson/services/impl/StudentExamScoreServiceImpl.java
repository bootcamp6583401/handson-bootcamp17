package task.two.lesson.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.StudentExamScore;
import task.two.lesson.repositories.StudentExamScoreRepository;

@Service
public class StudentExamScoreServiceImpl {

    @Autowired
    StudentExamScoreRepository repo;

    public Iterable<StudentExamScore> getAll() {
        return repo.findAll();
    }

    public StudentExamScore save(StudentExamScore address) {
        return repo.save(address);
    }

    public StudentExamScore getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("StudentExamScore not found"));
    }

    public StudentExamScore updateById(int id, StudentExamScore address) {
        Optional<StudentExamScore> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}