package task.two.lesson.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.Teacher;
import task.two.lesson.repositories.TeacherRepository;

@Service
public class TeacherServiceImpl {

    @Autowired
    TeacherRepository repo;

    public Iterable<Teacher> getAll() {
        return repo.findAll();
    }

    public Teacher save(Teacher address) {
        return repo.save(address);
    }

    public Teacher getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Teacher not found"));
    }

    public Teacher updateById(int id, Teacher address) {
        Optional<Teacher> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}