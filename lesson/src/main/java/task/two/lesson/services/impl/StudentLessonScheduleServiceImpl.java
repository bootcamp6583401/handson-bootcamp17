package task.two.lesson.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.StudentLessonSchedule;
import task.two.lesson.repositories.StudentLessonScheduleRepository;

@Service
public class StudentLessonScheduleServiceImpl {

    @Autowired
    StudentLessonScheduleRepository repo;

    public Iterable<StudentLessonSchedule> getAll() {
        return repo.findAll();
    }

    public StudentLessonSchedule save(StudentLessonSchedule address) {
        return repo.save(address);
    }

    public StudentLessonSchedule getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("StudentLessonSchedule not found"));
    }

    public StudentLessonSchedule updateById(int id, StudentLessonSchedule address) {
        Optional<StudentLessonSchedule> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}