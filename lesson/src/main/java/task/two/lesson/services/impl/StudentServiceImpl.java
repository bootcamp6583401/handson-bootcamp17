package task.two.lesson.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.Student;
import task.two.lesson.repositories.StudentRepository;

@Service
public class StudentServiceImpl {

    @Autowired
    StudentRepository repo;

    public Iterable<Student> getAll() {
        return repo.findAll();
    }

    public Student save(Student address) {
        return repo.save(address);
    }

    public Student getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Student not found"));
    }

    public Student updateById(int id, Student address) {
        Optional<Student> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    public void saveMale(String firstName, String lastName, String className) {
        repo.saveMale(firstName, lastName, className);
    }

    public Student getByFirstName(String firstName) {
        return repo.findAllByFirstName(firstName);
    }

}