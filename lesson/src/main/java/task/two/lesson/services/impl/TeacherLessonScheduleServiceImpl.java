package task.two.lesson.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.TeacherLessonSchedule;
import task.two.lesson.repositories.TeacherLessonScheduleRepository;

@Service
public class TeacherLessonScheduleServiceImpl {

    @Autowired
    TeacherLessonScheduleRepository repo;

    public Iterable<TeacherLessonSchedule> getAll() {
        return repo.findAll();
    }

    public TeacherLessonSchedule save(TeacherLessonSchedule address) {
        return repo.save(address);
    }

    public TeacherLessonSchedule getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("TeacherLessonSchedule not found"));
    }

    public TeacherLessonSchedule updateById(int id, TeacherLessonSchedule address) {
        Optional<TeacherLessonSchedule> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}