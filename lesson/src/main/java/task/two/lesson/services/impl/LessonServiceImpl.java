package task.two.lesson.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.Lesson;
import task.two.lesson.repositories.LessonRepository;

@Service
public class LessonServiceImpl {

    @Autowired
    LessonRepository repo;

    public Iterable<Lesson> getAll() {
        return repo.findAll();
    }

    public Lesson save(Lesson address) {
        return repo.save(address);
    }

    public Lesson getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Lesson not found"));
    }

    public Lesson updateById(int id, Lesson address) {
        Optional<Lesson> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}