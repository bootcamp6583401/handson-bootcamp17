package task.two.lesson.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import task.two.lesson.models.Exam;
import task.two.lesson.repositories.ExamRepository;

@Service
public class ExamServiceImpl {

    @Autowired
    ExamRepository repo;

    public Iterable<Exam> getAll() {
        return repo.findAll();
    }

    public Exam save(Exam address) {
        return repo.save(address);
    }

    public Exam getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Exam not found"));
    }

    public Exam updateById(int id, Exam address) {
        Optional<Exam> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}