package task.two.lesson.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import jakarta.transaction.Transactional;
import task.two.lesson.models.Student;

public interface StudentRepository extends CrudRepository<Student, Integer> {

    @Query(value = "INSERT INTO student (first_name, last_name, class_name, gender) VALUES (?1, ?2, ?3, 'MALE')", nativeQuery = true)
    @Transactional
    @Modifying
    public void saveMale(String firstName, String lastName, String className);

    @Query(value = "SELECT * FROM student where first_name = ?1 LIMIT 1", nativeQuery = true)
    public Student findAllByFirstName(String firstName);

    @Query(value = "SELECT * FROM student LIMIT 1", nativeQuery = true)
    public Student findOne();
}
