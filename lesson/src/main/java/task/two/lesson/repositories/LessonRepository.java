package task.two.lesson.repositories;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import jakarta.transaction.Transactional;
import task.two.lesson.models.Lesson;

public interface LessonRepository extends CrudRepository<Lesson, Integer> {
    @Query(value = "INSERT INTO lesson (class_name, subject) VALUES (?1, ?2)", nativeQuery = true)
    @Transactional
    @Modifying
    public void saveCustom(String className, String subject);

    @Query(value = "SELECT * FROM lesson where first_name = ?1 LIMIT 1", nativeQuery = true)
    public Lesson findOneByName(String className);

    @Query(value = "SELECT * FROM lesson LIMIT 1", nativeQuery = true)
    public Lesson findOne();
}
