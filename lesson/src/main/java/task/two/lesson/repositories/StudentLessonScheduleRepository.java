package task.two.lesson.repositories;

import org.springframework.data.repository.CrudRepository;

import task.two.lesson.models.StudentLessonSchedule;

public interface StudentLessonScheduleRepository extends CrudRepository<StudentLessonSchedule, Integer> {
}
