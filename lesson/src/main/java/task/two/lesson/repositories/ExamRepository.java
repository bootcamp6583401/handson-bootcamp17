package task.two.lesson.repositories;

import org.springframework.data.repository.CrudRepository;

import task.two.lesson.models.Exam;

public interface ExamRepository extends CrudRepository<Exam, Integer> {
}
