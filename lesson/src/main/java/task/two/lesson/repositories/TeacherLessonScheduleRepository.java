package task.two.lesson.repositories;

import org.springframework.data.repository.CrudRepository;

import task.two.lesson.models.TeacherLessonSchedule;

public interface TeacherLessonScheduleRepository extends CrudRepository<TeacherLessonSchedule, Integer> {
}
