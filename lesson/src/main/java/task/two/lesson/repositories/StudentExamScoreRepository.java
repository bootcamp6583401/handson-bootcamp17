package task.two.lesson.repositories;

import org.springframework.data.repository.CrudRepository;

import task.two.lesson.models.StudentExamScore;

public interface StudentExamScoreRepository extends CrudRepository<StudentExamScore, Integer> {
}
