package task.two.lesson.repositories;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import jakarta.transaction.Transactional;
import task.two.lesson.models.Teacher;

public interface TeacherRepository extends CrudRepository<Teacher, Integer> {
    @Query(value = "INSERT INTO teacher (first_name, last_name, birth_date, gender) VALUES (?1, ?2, ?3, ?4)", nativeQuery = true)
    @Transactional
    @Modifying
    public void saveCustom(String firstName, String lastName, LocalDate birthDate, String gender);

    @Query(value = "SELECT * FROM teacher where first_name = ?1 LIMIT 1", nativeQuery = true)
    public Teacher findOneByFirstName(String firstName);

    @Query(value = "SELECT * FROM teacher LIMIT 1", nativeQuery = true)
    public Teacher findOne();
}
